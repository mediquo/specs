#!/usr/bin/ruby

require 'dotenv/load'

Pod::Spec.new do |spec|
    spec.name = 'MediQuo-Socket'
    spec.version = '0.10'
    spec.summary = 'MediQuo Socket Framework for iOS'
    spec.authors = { "Medipremium, S.L." => "medipremium@gmail.com" }
    spec.license = { :type => 'Copyright', :text => 'Copyright © 2017 Medipremium S.L. All rights reserved.' }
    # spec.source = { :git => 'https://gitlab.com/mediquo/medi-ios.git', :tag => "#{spec.version}" }
    spec.source = { :http => "https://mediquo.bintray.com/generic/MediQuoSocket_0.10.zip" }
    spec.homepage = 'https://www.medipremium.com'
    spec.ios.deployment_target = '10.0'
    spec.platform = :ios, '10.0'

    spec.module_name = 'MediQuoSocket'
    spec.ios.frameworks = ['Foundation', 'UIKit']
    spec.ios.vendored_frameworks = 'MediQuo/MediQuoSocket.framework'

    spec.ios.dependency 'MediQuo-Core', "0.10"
    spec.ios.dependency 'MediQuo-Schema', "0.10"
    spec.ios.dependency 'MediQuo-Controller', "0.10"
    spec.ios.dependency 'Socket.IO-Client-Swift', '12.1.3'

    # spec.source_files = 'Socket/Socket/Source/**/*.{swift}'
end
