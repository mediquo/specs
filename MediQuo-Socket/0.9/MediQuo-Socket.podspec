Pod::Spec.new do |spec|
    spec.name = 'MediQuo-Socket'
    spec.version = '0.9'
    spec.summary = 'MediQuo Socket Framework for iOS'
    spec.authors = { "Medipremium, S.L." => "medipremium@gmail.com" }
    spec.license = { :type => 'Copyright', :text => 'Copyright © 2017 Medipremium S.L. All rights reserved.' }
    spec.source = { :http => 'https://mediquo.bintray.com/generic/Socket_0.9.zip' }
    spec.homepage = 'https://www.medipremium.com'
    spec.ios.deployment_target = '10.0'
    spec.platform = :ios, '10.0'

    spec.module_name = 'MediQuoSocket'
    # spec.preserve_paths = 'MediQuo/Socket.framework'
    # spec.source_files = 'Socket.framework/Headers/*.{h}'
    # spec.ios.public_header_files = 'Socket.framework/Headers/*.h'
    spec.ios.vendored_frameworks = 'MediQuo/Socket.framework'
    spec.ios.frameworks = ['Foundation', 'UIKit']
    spec.ios.dependency 'Socket.IO-Client-Swift', '~> 12.1.3'
    spec.ios.dependency 'RxSwift', '~> 4.0.0'
end
