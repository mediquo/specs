Pod::Spec.new do |spec|
    spec.name = 'MediQuo'
    spec.version = '0.7'
    spec.summary = 'MediQuo Framework for iOS'
    spec.authors = { "Medipremium, S.L." => "medipremium@gmail.com" }
    spec.license = { :type => 'Copyright', :text => 'Copyright © 2017 Medipremium S.L. All rights reserved.' }
    spec.source = { :http => 'https://mediquo.bintray.com/generic/MediQuo_0.7.zip' }
    spec.homepage = 'https://www.medipremium.com'
    spec.ios.deployment_target = '10.0'
    spec.platform = :ios, '10.0'

    spec.module_name = 'MediQuo'
    # spec.preserve_paths = 'MediQuo/MediQuo.framework'
    # spec.source_files = 'MediQuo.framework/Headers/*.{h}'
    # spec.ios.public_header_files = 'MediQuo.framework/Headers/*.h'
    spec.ios.vendored_frameworks = 'MediQuo/MediQuo.framework'
    spec.ios.frameworks = ['Foundation', 'CoreLocation', 'UIKit']
    spec.ios.dependency 'MediQuo-Core', '~> 0.7'
    spec.ios.dependency 'MediQuo-Schema', '~> 0.7'
    spec.ios.dependency 'MediQuo-Socket', '~> 0.7'
    spec.ios.dependency 'MediQuo-Remote', '~> 0.7'
    spec.ios.dependency 'MediQuo-Storage', '~> 0.7'
    spec.ios.dependency 'MediQuo-Controller', '~> 0.7'
    spec.ios.dependency 'Socket.IO-Client-Swift', '~> 12.1.2'
    spec.ios.dependency 'JSQDataSourcesKit', '~> 6.0.0'
    spec.ios.dependency 'AlamofireImage', '~> 3.3.0'
    spec.ios.dependency 'MessageKit', '~> 0.10.0'
    spec.ios.dependency 'RealmSwift', '~> 3.0.1'
    spec.ios.dependency 'Alamofire', '~> 4.5.1'
    spec.ios.dependency 'RxSwift', '~> 4.0.0'
end
