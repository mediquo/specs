#!/usr/bin/ruby

Pod::Spec.new do |spec|
    spec.name = 'MediQuo-Storage'
    spec.version = '0.10'
    spec.summary = 'MediQuo Storage Framework for iOS'
    spec.authors = { "Medipremium, S.L." => "medipremium@gmail.com" }
    spec.license = { :type => 'Copyright', :text => 'Copyright © 2017 Medipremium S.L. All rights reserved.' }
    # spec.source = { :git => 'https://gitlab.com/mediquo/medi-ios.git', :tag => "#{spec.version}" }
    spec.source = { :http => "https://mediquo.bintray.com/generic/MediQuoStorage_0.10.zip" }
    spec.homepage = 'https://www.medipremium.com'
    spec.ios.deployment_target = '10.0'
    spec.platform = :ios, '10.0'

    spec.module_name = 'MediQuoStorage'
    spec.ios.frameworks = ['Foundation', 'CoreLocation', 'UIKit']
    spec.ios.vendored_frameworks = 'MediQuo/MediQuoStorage.framework'

    spec.ios.dependency 'MediQuo-Core', "0.10"
    spec.ios.dependency 'MediQuo-Schema', "0.10"
    spec.ios.dependency 'MediQuo-Controller', "0.10"
    spec.ios.dependency 'MediQuo-Socket', "0.10"
    spec.ios.dependency 'RealmSwift', '3.0.2'

    # spec.source_files = 'Storage/Storage/Source/**/*.{swift}'
end
