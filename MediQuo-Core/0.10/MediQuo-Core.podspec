#!/usr/bin/ruby

Pod::Spec.new do |spec|
    spec.name = 'MediQuo-Core'
    spec.version = '0.10'
    spec.summary = 'MediQuo Core Framework for iOS'
    spec.authors = { "Medipremium, S.L." => "medipremium@gmail.com" }
    spec.license = { :type => 'Copyright', :text => 'Copyright © 2017 Medipremium S.L. All rights reserved.' }
    # spec.source = { :git => 'https://gitlab.com/mediquo/medi-ios.git', :tag => "#{spec.version}" }
    spec.source = { :http => "https://mediquo.bintray.com/generic/MediQuoCore_0.10.zip" }
    spec.homepage = 'https://www.medipremium.com'
    spec.ios.deployment_target = '10.0'
    spec.platform = :ios, '10.0'

    spec.module_name = 'MediQuoCore'
    spec.ios.frameworks = ['Foundation', 'UIKit']
    spec.ios.vendored_frameworks = 'MediQuo/MediQuoCore.framework'

    # spec.source_files = 'Core/Core/Source/**/*.{swift}'
    # spec.resource_bundles = {
    #    'MediQuo' => ['Core/Core/Resource/**/*.{stencil}']
    # }
end
