Pod::Spec.new do |spec|
    spec.name = 'MediQuo-Core'
    spec.version = '0.9'
    spec.summary = 'MediQuo Core Framework for iOS'
    spec.authors = { "Medipremium, S.L." => "medipremium@gmail.com" }
    spec.license = { :type => 'Copyright', :text => 'Copyright © 2017 Medipremium S.L. All rights reserved.' }
    spec.source = { :http => 'https://mediquo.bintray.com/generic/Core_0.9.zip' }
    spec.homepage = 'https://www.medipremium.com'
    spec.ios.deployment_target = '10.0'
    spec.platform = :ios, '10.0'

    spec.module_name = 'MediQuoCore'
    # spec.preserve_paths = 'MediQuo/Core.framework'
    # spec.source_files = 'Core.framework/Headers/*.{h}'
    # spec.ios.public_header_files = 'Core.framework/Headers/*.h'
    spec.ios.vendored_frameworks = 'MediQuo/Core.framework'
    spec.ios.frameworks = ['Foundation', 'UIKit']
end
