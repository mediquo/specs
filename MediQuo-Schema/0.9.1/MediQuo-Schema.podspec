Pod::Spec.new do |spec|
    spec.name = 'MediQuo-Schema'
    spec.version = '0.9.1'
    spec.summary = 'MediQuo Schema Framework for iOS'
    spec.authors = { "Medipremium, S.L." => "medipremium@gmail.com" }
    spec.license = { :type => 'Copyright', :text => 'Copyright © 2017 Medipremium S.L. All rights reserved.' }
    spec.source = { :http => "https://mediquo.bintray.com/generic/Schema_0.9.1.zip" }
    spec.homepage = 'https://www.medipremium.com'
    spec.ios.deployment_target = '10.0'
    spec.platform = :ios, '10.0'

    spec.module_name = 'MediQuoSchema'
    spec.ios.vendored_frameworks = 'MediQuo/Schema.framework'
    spec.ios.frameworks = ['Foundation', 'UIKit']
end
